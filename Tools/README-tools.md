# Kite Guitar related tools (41 Equal temperament) and how to use them:

## Online Playable Keyboards 
no extra hardware required besides keyboard and computer
- [Sevish](https://sevish.com/scaleworkshop/?name=41%20equal%20divisions%20of%202%2F1&data=29.26829268292683%0A58.53658536585366%0A87.8048780487805%0A117.07317073170732%0A146.34146341463415%0A175.609756097561%0A204.8780487804878%0A234.14634146341464%0A263.4146341463415%0A292.6829268292683%0A321.9512195121951%0A351.219512195122%0A380.4878048780488%0A409.7560975609756%0A439.0243902439025%0A468.2926829268293%0A497.5609756097561%0A526.829268292683%0A556.0975609756098%0A585.3658536585366%0A614.6341463414634%0A643.9024390243902%0A673.1707317073171%0A702.439024390244%0A731.7073170731708%0A760.9756097560976%0A790.2439024390244%0A819.5121951219512%0A848.7804878048781%0A878.048780487805%0A907.3170731707318%0A936.5853658536586%0A965.8536585365854%0A995.1219512195122%0A1024.3902439024391%0A1053.658536585366%0A1082.9268292682927%0A1112.1951219512196%0A1141.4634146341464%0A1170.7317073170732%0A1200.&freq=440.621&midi=69&vert=13&horiz=2&colors=white%20black%20white%20white%20black%20white%20black%20white%20white%20black%20white%20black&waveform=semisine&ampenv=organ)





## http://www.tallkite.com/alt-tuner.html: 
Alt-tuner is a DAW plug-in that retunes almost every midi keyboad or softsynth (developed by Kite Giedraitis) - can be used for the 41-EDO Kite Tuning as well as others.

## 41EDO Ear Trainer (by Kite Giedraitis)
- a Reaper plugin with fantastic features

# 41-edo.scl is a scala file in this repo for use with any scala compatible program
- Many of them are listed at https://en.xen.wiki/w/List_of_Microtonal_Software_Plugins
but only the ones that say "scl file import" under "tuning method" apply


Tuners:

EDOTUNER (folder in this repository) is a custom strobe tuner (made by Kite) for Reaper - 
download and follow the instructions at the top of EDOTUNER.txt in order to load and use

Lingot is a tuning software with a branch made by ibancg which imports the labels properly from the 41-edo.scl file,
you can find it here:
https://github.com/ibancg/lingot  (note: you will have to change the deviation of cents)

'





Please also refer to other primary resources at:

www.kiteguitar.com

www.tallkite.com

https://en.xen.wiki/w/The_Kite_Guitar

