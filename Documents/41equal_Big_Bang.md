# Explaining Kite-fretting as 12-divisions of P5

- Regular guitar frets divide the octave (2/1 harmonic ratio) into 12 equal steps.
- Kite-fretting divides the perfect fifth (3/2 harmonic ratio) into 12 equal steps.
- Strings are tuned 6.5 frets apart.
    - That's a down (i.e. small) major third (a well-tuned harmonic 5/4 ratio).
- So instead of duplicate notes, each string has pitches half-way between the ones on the next string.
- On a pair of strings, we have 24 equal divisions of the 3/2 perfect fifth.
- A perfect fourth (harmonic ratio 4/3) works out to be 17 of those steps.
- 24 + 17 = 41 notes per octave (since P5 + P4 = P8, or in ratios, 3/2 * 4/3 = 2/1)
- Full name of the tuning is "41 Equal Divisions of the Octave", for short: "41edo" or "41 equal" [^pickymath]
- Each step in an edo is called an "edostep", so the 6.5 fret interval between strings is 13 edosteps.
- Fretting a subset of an edo and spreading the pitches across multiple strings is called "skip-fretting".
- So, Kite-fretting is 41/2 (half of 41edo) skip-fretting.

Other skip-frettings are possible (and music of some sort is possible with any tuning system), but *only* Kite-fretting provides effective Just Intonation with near-precise harmonic tuning and all the primary blended intervals in an accessible layout.

[^pickymath]: 41 divisions of the octave and 24 divisions of the perfect fifth are not *precisely* identical, but the difference is a imperceptible, a negligible rounding-error.
